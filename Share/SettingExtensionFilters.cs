﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Share
{
    [Serializable]
  [DataContract] 
    public class SettingExtensionFilters
    {
      [DataMember]
     // public List<string> ExtensionFilters { get; set; }

      private List<string> ExtensionFilters = null;
       public SettingExtensionFilters()
        {
            this.ExtensionFilters = new List<string>();
        }

        public List<string> getExtensionFilters()
        {
            return this.ExtensionFilters;

        }
        public void addExtensionFilters(string argExtensionFilters)
        {
           this.ExtensionFilters.Add(argExtensionFilters);

        }
        public void deleteExtensionFilters(string argExtensionFilters)
        {
            this.ExtensionFilters.Remove(argExtensionFilters);
        }

    }
}
