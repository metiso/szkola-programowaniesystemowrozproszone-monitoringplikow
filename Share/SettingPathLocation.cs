﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;
namespace Share
{
    [Serializable]
    [DataContract]
    public class SettingPathLocation
    {
        [DataMember]
        private string PathLocation = "%USERPROFILE%";
        [DataMember]
        private bool IncludeSubdirectories = false;
        [DataMember]
        private List<string> ListPath = new List<string>();
        public SettingPathLocation()
        {
           // ListPath.Add(PathLocation);
        }
        // public string PathLocation { get; set; }
        public void AddPath(string argPath)
        {
            ListPath.Add(argPath);
        }
        public void removePath(string argPath)
        {
            ListPath.Remove(argPath);
        }

        public List<String> getListPath()
        {
            return ListPath;
        }
        public string getPathLocation()
        {
            return this.PathLocation;
        }

        public void setPathLocation(string argPathLocation)
        {
            this.PathLocation = argPathLocation;

        }
        public bool getIncludeSubdirectories()
        {
            return this.IncludeSubdirectories;
        }
        public void setIncludeSubdirectories(bool argIncludeSubdirectories)
        {
            this.IncludeSubdirectories = argIncludeSubdirectories;

        }

    }
}
