﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;
namespace Share
{
    //[DataContract] 
    public class FileEntity
    {
        public FileEntity()
        {
        }
        public FileEntity(string ArgNameFile, string ArgHashFile, DateTime ArgFileTime, string ArgUserName, string ArgComputerName)
        {
            this.nameFile = ArgNameFile;
            this.hashFile = ArgHashFile;
            this.FileTime = ArgFileTime;
            this.UserName = ArgUserName;
            this.ComputerName = ArgComputerName;
        }
        public string nameFile { get; set; }
        public string hashFile { get; set; }
        public string UserName { get; set; }
        public DateTime FileTime { get; set; }
        public string ComputerName { get; set; }
        public int versionFile { get; set; }
        public string FileContent { get; set; }
    }

}
