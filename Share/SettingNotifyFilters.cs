﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;
namespace Share
{
    [Serializable]
    [DataContract]
    public class SettingNotifyFilters
    {

        [DataMember]

        //List<>
        // public NotifyFilters notifyFilters = 0;
        private NotifyFilters notifyFilters = 0;



        //void getActiveListFilters();
        // void getAllListFilters();

        public void addNotifyFilters(NotifyFilters argNotifyFilters)
        {
            this.notifyFilters |= argNotifyFilters;

        }
        public NotifyFilters getNotifyFilters()
        {
            return this.notifyFilters;

        }
        public void deleteNotifyFilters(NotifyFilters argNotifyFilters)
        {
            this.notifyFilters ^= argNotifyFilters;
        }
        public bool checkNotifyFilters(NotifyFilters argNotifyFilters)
        {
            NotifyFilters tmp;
            tmp = this.notifyFilters;

            this.notifyFilters |= argNotifyFilters;
            if (tmp == this.notifyFilters)
            {
                this.notifyFilters = tmp;
                return true;
            }
            else
            {
                this.notifyFilters = tmp;
                return false;
            }


        }
        // public NotifyFilters notifyFilters { get; set; }


    }
}

/*
Attributes	
Atrybuty pliku lub folderu.

CreationTime	
Godzina utworzenia pliku lub folderu.

DirectoryName	
Nazwa katalogu.

FileName	
Nazwa pliku.

LastAccess	
Data ostatniego otwarcia pliku lub folderu.

LastWrite	
Data pliku lub folderu ostatnie wysłał zapisanych.

Security	
Ustawienia zabezpieczeń pliku lub folderu.

Size	
Rozmiar pliku lub folderu.*/