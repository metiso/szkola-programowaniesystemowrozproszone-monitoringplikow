﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Share
{
    [Serializable]
    public class SettingUserEntity
    {
        public SettingUserEntity()
        {
        }
        public SettingUserEntity(SettingExtensionFilters ArgSEF, SettingNotifyFilters ArgSNF, SettingPathLocation ArgSPL)
        {
             this.SettingExtensionFilters = ArgSEF;
             this.SettingNotifyFilters = ArgSNF;
             this.SettingPathLocation = ArgSPL;
        }
        public SettingExtensionFilters SettingExtensionFilters { get; set; }
       
        public SettingNotifyFilters SettingNotifyFilters { get; set; }
        public SettingPathLocation SettingPathLocation { get; set; }
        
    }

}
