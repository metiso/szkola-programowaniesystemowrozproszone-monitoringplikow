﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MonitoringAdmin
{
class passwordBox
{
    private Form frm;
    public string Show(string prompt, string title)
    {
        frm = new Form();
        FlowLayoutPanel FL = new FlowLayoutPanel();
        Label lbl = new Label();
        TextBox txt = new TextBox();
        Button ok = new Button();
        Button cancel = new Button();

        frm.Font = new Font("Calibri", 9, FontStyle.Bold);
        frm.FormBorderStyle = FormBorderStyle.FixedToolWindow;
        frm.StartPosition = FormStartPosition.CenterScreen;
        frm.Width = 200;
        frm.Height = 160;

        frm.Text = title;
        lbl.Text = prompt;
        ok.Text = "Ok";
        cancel.Text = "Cancel";
        txt.PasswordChar = '*';

        ok.FlatStyle = FlatStyle.Flat;
        ok.BackColor = SystemColors.ButtonShadow;
        ok.ForeColor = SystemColors.ButtonHighlight;
        ok.Cursor = Cursors.Hand;

        cancel.FlatStyle = FlatStyle.Flat;
        cancel.BackColor = SystemColors.ButtonShadow;
        cancel.ForeColor = SystemColors.ButtonHighlight;
        cancel.Cursor = Cursors.Hand;

        FL.Left = 0;
        FL.Top = 0;
        FL.Width = frm.Width;
        FL.Height = frm.Height;
        FL.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
        FL.Padding = new Padding(10);
        FL.FlowDirection = FlowDirection.TopDown;

        ok.Width = FL.Width-35;
        txt.Width = ok.Width;
        cancel.Width = ok.Width;
        lbl.Width = ok.Width;

        ok.Click += new System.EventHandler(okClick);
        cancel.Click += new System.EventHandler(cancelClick);
        txt.KeyPress += new KeyPressEventHandler(txtEnter);

        FL.Controls.Add(lbl);
        FL.Controls.Add(txt);
        FL.Controls.Add(ok);
        FL.Controls.Add(cancel);
        frm.Controls.Add(FL);

        frm.ShowDialog();
        DialogResult DR = frm.DialogResult;
        frm.Dispose();
       // frm = null;
        if (DR == DialogResult.OK)
        {
            string hash = "";
            using (MD5 md5Hash = MD5.Create())
            {
               hash = GetMd5Hash(md5Hash, txt.Text);
           }

            return hash;
        }
        else
        {
            return "";
        }
    }
    static string GetMd5Hash(MD5 md5Hash, string input)
    {

        // Convert the input string to a byte array and compute the hash.
        byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

        // Create a new Stringbuilder to collect the bytes
        // and create a string.
        StringBuilder sBuilder = new StringBuilder();

        // Loop through each byte of the hashed data 
        // and format each one as a hexadecimal string.
        for (int i = 0; i < data.Length; i++)
        {
            sBuilder.Append(data[i].ToString("x2"));
        }

        // Return the hexadecimal string.
        return sBuilder.ToString();
    }
    private void okClick(object sender, System.EventArgs e)
    {
        frm.DialogResult = DialogResult.OK;
        frm.Close();
    }
    private void cancelClick(object sender, System.EventArgs e)
    {
        frm.DialogResult = DialogResult.Cancel;
        frm.Close();
        
    }
    private void txtEnter(object sender, KeyPressEventArgs e)
    {
        if (e.KeyChar == 13) { okClick(null, null); }
    }
}
}
