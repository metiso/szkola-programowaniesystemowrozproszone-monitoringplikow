﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Discovery;
//using DiscaveryClientApp.MonitoringServiceReference1;
using System.Collections.ObjectModel;
using System.IO;
using MonitoringSerwerWCF;

namespace MonitoringAdmin
{

    class ConnectService
    {
        public static string Password { get; set; }
        
        static IMonitoring proxy = null;
        private static IMonitoring ConnectWCF()
        {
            if (proxy == null)
            {
                var client = new DiscoveryClient(new UdpDiscoveryEndpoint());
                var criteria = new FindCriteria(typeof(IMonitoring));
                criteria.MaxResults = 1;
                var findResult = client.Find(criteria);


                Console.WriteLine();
                var address = findResult.Endpoints.First(ep => ep.Address.Uri.Scheme == "http").Address;
                Console.WriteLine(address);
                var factory = new ChannelFactory<IMonitoring>(new BasicHttpBinding(), address);
                proxy = factory.CreateChannel();
                return proxy;
            }
            return proxy;

        }

        public static void closeConnectWCF()
        {
            //Console.WriteLine(ConnectService.getConnectWCF().checkPasswordProgram("").ToString());
            // ((ICommunicationObject)proxy.Close();
        }

        public static IMonitoring getConnectWCF()
        {
            return ConnectWCF();
        }
        static void Main()
        {
        }
        
    }
}