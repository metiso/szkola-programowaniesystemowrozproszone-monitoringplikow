﻿using System.Collections;
using System.Windows.Forms;
namespace MonitoringAdmin
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.asd = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.listView2 = new System.Windows.Forms.ListView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button10 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkBoxSize = new System.Windows.Forms.CheckBox();
            this.checkBoxLastAccess = new System.Windows.Forms.CheckBox();
            this.checkBoxLastWrite = new System.Windows.Forms.CheckBox();
            this.checkBoxAttributes = new System.Windows.Forms.CheckBox();
            this.checkBoxCreationTime = new System.Windows.Forms.CheckBox();
            this.checkBoxDirectoryName = new System.Windows.Forms.CheckBox();
            this.checkBoxFileName = new System.Windows.Forms.CheckBox();
            this.checkBoxSecurity = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.listView4 = new System.Windows.Forms.ListView();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.listView3 = new System.Windows.Forms.ListView();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.eventLog1 = new System.Diagnostics.EventLog();
            this.eventLog2 = new System.Diagnostics.EventLog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.asd.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.eventLog1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventLog2)).BeginInit();
            this.SuspendLayout();
            // 
            // asd
            // 
            this.asd.Controls.Add(this.tabPage1);
            this.asd.Controls.Add(this.tabPage2);
            this.asd.Location = new System.Drawing.Point(2, 2);
            this.asd.Name = "asd";
            this.asd.SelectedIndex = 0;
            this.asd.Size = new System.Drawing.Size(737, 505);
            this.asd.TabIndex = 8;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button9);
            this.tabPage1.Controls.Add(this.button8);
            this.tabPage1.Controls.Add(this.listView2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(729, 479);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Monitoring";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Enter += new System.EventHandler(this.tabPage1_Enter);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(317, 441);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(406, 32);
            this.button9.TabIndex = 2;
            this.button9.Text = "odśwież";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(0, 441);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(311, 32);
            this.button8.TabIndex = 1;
            this.button8.Text = "Zobacz Plik";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // listView2
            // 
            this.listView2.Location = new System.Drawing.Point(3, 6);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(720, 429);
            this.listView2.TabIndex = 0;
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.Column2Click);
            this.listView2.SelectedIndexChanged += new System.EventHandler(this.listView2_SelectedIndexChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button10);
            this.tabPage2.Controls.Add(this.button7);
            this.tabPage2.Controls.Add(this.button6);
            this.tabPage2.Controls.Add(this.button5);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.button4);
            this.tabPage2.Controls.Add(this.textBox2);
            this.tabPage2.Controls.Add(this.listView4);
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.textBox1);
            this.tabPage2.Controls.Add(this.listView3);
            this.tabPage2.Controls.Add(this.comboBox1);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.listView1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(729, 479);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Ustawienia Klientow";
            this.tabPage2.UseVisualStyleBackColor = true;
            this.tabPage2.Enter += new System.EventHandler(this.tabPage2_Enter);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(10, 444);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(220, 26);
            this.button10.TabIndex = 28;
            this.button10.Text = "odswież";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(595, 447);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(127, 23);
            this.button7.TabIndex = 25;
            this.button7.Text = "Usuń";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(322, 267);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(68, 24);
            this.button6.TabIndex = 24;
            this.button6.Text = "Usun";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(462, 447);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(127, 23);
            this.button5.TabIndex = 23;
            this.button5.Text = "Wpisz";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBoxSize);
            this.groupBox1.Controls.Add(this.checkBoxLastAccess);
            this.groupBox1.Controls.Add(this.checkBoxLastWrite);
            this.groupBox1.Controls.Add(this.checkBoxAttributes);
            this.groupBox1.Controls.Add(this.checkBoxCreationTime);
            this.groupBox1.Controls.Add(this.checkBoxDirectoryName);
            this.groupBox1.Controls.Add(this.checkBoxFileName);
            this.groupBox1.Controls.Add(this.checkBoxSecurity);
            this.groupBox1.Location = new System.Drawing.Point(240, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(213, 122);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ustawienia Atrybutów";
            // 
            // checkBoxSize
            // 
            this.checkBoxSize.AutoSize = true;
            this.checkBoxSize.Location = new System.Drawing.Point(113, 88);
            this.checkBoxSize.Name = "checkBoxSize";
            this.checkBoxSize.Size = new System.Drawing.Size(46, 17);
            this.checkBoxSize.TabIndex = 10;
            this.checkBoxSize.Text = "\tSize";
            this.checkBoxSize.UseVisualStyleBackColor = true;
            // 
            // checkBoxLastAccess
            // 
            this.checkBoxLastAccess.AutoSize = true;
            this.checkBoxLastAccess.Location = new System.Drawing.Point(113, 19);
            this.checkBoxLastAccess.Name = "checkBoxLastAccess";
            this.checkBoxLastAccess.Size = new System.Drawing.Size(81, 17);
            this.checkBoxLastAccess.TabIndex = 7;
            this.checkBoxLastAccess.Text = "LastAccess";
            this.checkBoxLastAccess.UseVisualStyleBackColor = true;
            // 
            // checkBoxLastWrite
            // 
            this.checkBoxLastWrite.AutoSize = true;
            this.checkBoxLastWrite.Location = new System.Drawing.Point(113, 42);
            this.checkBoxLastWrite.Name = "checkBoxLastWrite";
            this.checkBoxLastWrite.Size = new System.Drawing.Size(71, 17);
            this.checkBoxLastWrite.TabIndex = 8;
            this.checkBoxLastWrite.Text = "LastWrite";
            this.checkBoxLastWrite.UseVisualStyleBackColor = true;
            // 
            // checkBoxAttributes
            // 
            this.checkBoxAttributes.AutoSize = true;
            this.checkBoxAttributes.Location = new System.Drawing.Point(6, 19);
            this.checkBoxAttributes.Name = "checkBoxAttributes";
            this.checkBoxAttributes.Size = new System.Drawing.Size(70, 17);
            this.checkBoxAttributes.TabIndex = 3;
            this.checkBoxAttributes.Text = "Attributes";
            this.checkBoxAttributes.UseVisualStyleBackColor = true;
            // 
            // checkBoxCreationTime
            // 
            this.checkBoxCreationTime.AutoSize = true;
            this.checkBoxCreationTime.Location = new System.Drawing.Point(6, 42);
            this.checkBoxCreationTime.Name = "checkBoxCreationTime";
            this.checkBoxCreationTime.Size = new System.Drawing.Size(88, 17);
            this.checkBoxCreationTime.TabIndex = 4;
            this.checkBoxCreationTime.Text = "CreationTime";
            this.checkBoxCreationTime.UseVisualStyleBackColor = true;
            // 
            // checkBoxDirectoryName
            // 
            this.checkBoxDirectoryName.AutoSize = true;
            this.checkBoxDirectoryName.Location = new System.Drawing.Point(6, 65);
            this.checkBoxDirectoryName.Name = "checkBoxDirectoryName";
            this.checkBoxDirectoryName.Size = new System.Drawing.Size(96, 17);
            this.checkBoxDirectoryName.TabIndex = 5;
            this.checkBoxDirectoryName.Text = "DirectoryName";
            this.checkBoxDirectoryName.UseVisualStyleBackColor = true;
            // 
            // checkBoxFileName
            // 
            this.checkBoxFileName.AutoSize = true;
            this.checkBoxFileName.Location = new System.Drawing.Point(6, 88);
            this.checkBoxFileName.Name = "checkBoxFileName";
            this.checkBoxFileName.Size = new System.Drawing.Size(70, 17);
            this.checkBoxFileName.TabIndex = 6;
            this.checkBoxFileName.Text = "FileName";
            this.checkBoxFileName.UseVisualStyleBackColor = true;
            // 
            // checkBoxSecurity
            // 
            this.checkBoxSecurity.AutoSize = true;
            this.checkBoxSecurity.Location = new System.Drawing.Point(113, 65);
            this.checkBoxSecurity.Name = "checkBoxSecurity";
            this.checkBoxSecurity.Size = new System.Drawing.Size(64, 17);
            this.checkBoxSecurity.TabIndex = 9;
            this.checkBoxSecurity.Text = "Security";
            this.checkBoxSecurity.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(237, 141);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Rozszerzenie";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(236, 299);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Path";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(240, 267);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(76, 24);
            this.button4.TabIndex = 19;
            this.button4.Text = "Dodaj";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(240, 241);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(150, 20);
            this.textBox2.TabIndex = 18;
            // 
            // listView4
            // 
            this.listView4.Location = new System.Drawing.Point(239, 157);
            this.listView4.Name = "listView4";
            this.listView4.Size = new System.Drawing.Size(151, 78);
            this.listView4.TabIndex = 17;
            this.listView4.UseCompatibleStateImageBehavior = false;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(595, 418);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(127, 23);
            this.button3.TabIndex = 16;
            this.button3.Text = "Dodaj";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(236, 418);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(353, 20);
            this.textBox1.TabIndex = 15;
            // 
            // listView3
            // 
            this.listView3.Location = new System.Drawing.Point(236, 315);
            this.listView3.Name = "listView3";
            this.listView3.Size = new System.Drawing.Size(487, 97);
            this.listView3.TabIndex = 14;
            this.listView3.UseCompatibleStateImageBehavior = false;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "%USERPROFILE%",
            "%USERDESKTOP%"});
            this.comboBox1.Location = new System.Drawing.Point(236, 447);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(217, 21);
            this.comboBox1.TabIndex = 12;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(562, 48);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(160, 35);
            this.button2.TabIndex = 11;
            this.button2.Text = "Wyslji ustawienia do wszysktich";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(563, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(160, 35);
            this.button1.TabIndex = 10;
            this.button1.Text = "Zapisz ustawienia";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // listView1
            // 
            this.listView1.Location = new System.Drawing.Point(10, 6);
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(224, 432);
            this.listView1.TabIndex = 2;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.ColumnClick);
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // eventLog1
            // 
            this.eventLog1.SynchronizingObject = this;
            // 
            // eventLog2
            // 
            this.eventLog2.SynchronizingObject = this;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(751, 519);
            this.Controls.Add(this.asd);
            this.Name = "Form1";
            this.Text = "Admin- Monitoring Plikow";
            this.asd.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.eventLog1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventLog2)).EndInit();
            this.ResumeLayout(false);

        }
        private void ColumnClick(object o, ColumnClickEventArgs e)
        {
            // Set the ListViewItemSorter property to a new ListViewItemComparer 
            // object. Setting this property immediately sorts the 
            // ListView using the ListViewItemComparer object.
            this.listView1.ListViewItemSorter = new ListViewItemComparer(e.Column);
        }
        private void Column2Click(object o, ColumnClickEventArgs e)
        {
            // Set the ListViewItemSorter property to a new ListViewItemComparer 
            // object. Setting this property immediately sorts the 
            // ListView using the ListViewItemComparer object.
            this.listView2.ListViewItemSorter = new ListViewItemComparer(e.Column);
        }
        class ListViewItemComparer : IComparer
        {
            private int col;
            public ListViewItemComparer()
            {
                col = 0;
            }
            public ListViewItemComparer(int column)
            {
                col = column;
            }
            public int Compare(object x, object y)
            {
                return string.Compare(((ListViewItem)x).SubItems[col].Text, ((ListViewItem)y).SubItems[col].Text);
            }
        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.TabControl asd;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox checkBoxSecurity;
        private System.Windows.Forms.CheckBox checkBoxLastWrite;
        private System.Windows.Forms.CheckBox checkBoxLastAccess;
        private System.Windows.Forms.CheckBox checkBoxFileName;
        private System.Windows.Forms.CheckBox checkBoxDirectoryName;
        private System.Windows.Forms.CheckBox checkBoxCreationTime;
        private System.Windows.Forms.CheckBox checkBoxAttributes;
        private System.Windows.Forms.ListView listView1;
        private System.Diagnostics.EventLog eventLog1;
        private System.Diagnostics.EventLog eventLog2;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ListView listView3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.ListView listView4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.CheckBox checkBoxSize;
        private Button button7;
        private Button button6;
        private Button button10;
        private Button button8;
        private Button button9;
    }
}

