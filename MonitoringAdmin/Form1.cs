﻿

using System.Windows.Forms;
using System;
using Share;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using MonitoringSerwerWCF;
using System.Collections.Specialized;
//using MonitoringAdmin.ServiceReference1;

namespace MonitoringAdmin
{
    public partial class Form1 : Form
    {
        UserEntity LastReadUser = null;
        String LastReadSimilarHash = null;
        private IMonitoring MC_oInstance = null;
        public IMonitoring MonitoringClientInstance
        {
            get
            {
                if (MC_oInstance == null)
                {
                    MC_oInstance = ConnectService.getConnectWCF();
                }
                return MC_oInstance;

            }
        }
        public Form1()
        {

            IMonitoring ms = MonitoringClientInstance;

            InitializeComponent();
            ClearAllTab();


        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (LastReadUser != null)
                {
                    //atribute
                    SettingNotifyFilters SNF = new SettingNotifyFilters();
                    if (checkBoxAttributes.CheckState == CheckState.Checked)
                        SNF.addNotifyFilters(NotifyFilters.Attributes);
                    if (checkBoxCreationTime.CheckState == CheckState.Checked)
                        SNF.addNotifyFilters(NotifyFilters.CreationTime);
                    if (checkBoxDirectoryName.CheckState == CheckState.Checked)
                        SNF.addNotifyFilters(NotifyFilters.DirectoryName);
                    if (checkBoxFileName.CheckState == CheckState.Checked)
                        SNF.addNotifyFilters(NotifyFilters.FileName);
                    if (checkBoxLastAccess.CheckState == CheckState.Checked)
                        SNF.addNotifyFilters(NotifyFilters.LastAccess);
                    if (checkBoxLastWrite.CheckState == CheckState.Checked)
                        SNF.addNotifyFilters(NotifyFilters.LastWrite);
                    if (checkBoxSecurity.CheckState == CheckState.Checked)
                        SNF.addNotifyFilters(NotifyFilters.Security);
                    if (checkBoxSize.CheckState == CheckState.Checked)
                        SNF.addNotifyFilters(NotifyFilters.Size);


                    //extens
                    SettingExtensionFilters SEF = new SettingExtensionFilters();
                    foreach (var str in listView4.Items)
                    {
                        ListViewItem list = (ListViewItem)str;
                        SEF.addExtensionFilters(list.Text);
                    }


                    //extens
                    SettingPathLocation PTL = new SettingPathLocation();
                    foreach (var str in listView3.Items)
                    {
                        ListViewItem list = (ListViewItem)str;
                        PTL.AddPath(list.Text);
                    }


                    SettingUserEntity SUE = new SettingUserEntity(SEF, SNF, PTL);
                    UserEntity UE = new UserEntity(LastReadUser.UserName, LastReadUser.ComputerName, SUE);
                    MonitoringClientInstance.SetSetting(ConnectService.Password, UE);


                    tabPage2_Enter(null, null);

                    //listView1.SelectedItems[0].Tag = UE;
                    //LastReadUser = myObj;
                }
                else
                {
                    MessageBox.Show("Zaznacz klienta.");
                }
            }
            catch
            {
                MessageBox.Show("Utracono polaczenie z serwerem!");
            }
        }
        private void ClearAllTab()
        {
            checkBoxAttributes.CheckState = CheckState.Unchecked;
            checkBoxCreationTime.CheckState = CheckState.Unchecked;
            checkBoxDirectoryName.CheckState = CheckState.Unchecked;
            checkBoxFileName.CheckState = CheckState.Unchecked;
            checkBoxLastAccess.CheckState = CheckState.Unchecked;
            checkBoxLastWrite.CheckState = CheckState.Unchecked;
            checkBoxSecurity.CheckState = CheckState.Unchecked;
            checkBoxSize.CheckState = CheckState.Unchecked;
            // checkBoxSecurity.CheckState == CheckState.Checked

            listView2.Clear();
            listView2.View = View.Details;
            listView2.GridLines = true;
            listView2.FullRowSelect = true;

            //Add column header
            listView2.Columns.Add("A-Uzytkownik", 100);
            listView2.Columns.Add("A-Komputer", 70);
            listView2.Columns.Add("A-Plik", 70);
            listView2.Columns.Add("A-Data", 115);
            listView2.Columns.Add("B-Uzytkownik", 100);
            listView2.Columns.Add("B-Komputer", 70);
            listView2.Columns.Add("B-Plik", 70);
            listView2.Columns.Add("B-Data", 115);


            // Add a column with width 20 and left alignment.
            listView1.Clear();



            listView1.View = View.Details;
            listView1.GridLines = true;
            listView1.FullRowSelect = true;

            //Add column header
            listView1.Columns.Add("Uzytkownik", 100);
            listView1.Columns.Add("Nazwa Komputera", 70);



            listView4.Clear();
            listView4.View = View.Details;
            listView4.GridLines = true;
            listView4.FullRowSelect = true;

            //Add column header
            listView4.Columns.Add("Rozszerzenia", 100);

            listView3.Clear();
            listView3.View = View.Details;
            listView3.GridLines = true;
            listView3.FullRowSelect = true;

            //Add column header
            listView3.Columns.Add("Path", 100);

        }

        private void tabPage1_Enter(object sender, EventArgs e)
        {
            button9_Click(null, null);
        }
        private void tabPage2_Enter(object sender, EventArgs e)
        {


            ClearAllTab();



            try
            {


                var Users = MonitoringClientInstance.GetUser(ConnectService.Password);
                foreach (var user in Users)
                {
                    ListViewItem item = new ListViewItem();
                    item.Text = user.UserName;
                    item.SubItems.Add(user.ComputerName);
                    item.Tag = user;
                    listView1.Items.Add(item);

                }
            }
            catch
            {
                MessageBox.Show("Utracono polaczenie z serwerem!");
            }
        }

        public bool checkNotifyFilters(NotifyFilters argNotifyFilters, NotifyFilters argNotifyAtribut)
        {
            NotifyFilters tmp;
            tmp = argNotifyFilters;

            argNotifyFilters |= argNotifyAtribut;
            if (tmp == argNotifyFilters)
            {
                return true;
            }
            else
            {
                return false;
            }


        }

        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            if (listView2.SelectedItems.Count > 0)
            {
               
                String myObj = (string)listView2.SelectedItems[0].Tag;
               // MessageBox.Show(myObj);
                LastReadSimilarHash = myObj;
                
            }
        }
       // private FileEntity getFileContent(String ArgHashFile)
       // {

           // return new FileEntity();
      //  }
        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (listView1.SelectedItems.Count > 0)
            {

                Share.UserEntity myObj = (Share.UserEntity)listView1.SelectedItems[0].Tag;
                LastReadUser = myObj;
                ///string listItem = listView1.SelectedItems[0].Text;
                //textBox1.Text = listItem;

                SettingNotifyFilters SNF = myObj.setting.SettingNotifyFilters;


                //Load Notify
                if (checkNotifyFilters(SNF.getNotifyFilters(), NotifyFilters.Attributes))
                    checkBoxAttributes.CheckState = CheckState.Checked;
                else
                    checkBoxAttributes.CheckState = CheckState.Unchecked;

                if (checkNotifyFilters(SNF.getNotifyFilters(), NotifyFilters.CreationTime))
                    checkBoxCreationTime.CheckState = CheckState.Checked;
                else
                    checkBoxCreationTime.CheckState = CheckState.Unchecked;

                if (checkNotifyFilters(SNF.getNotifyFilters(), NotifyFilters.DirectoryName))
                    checkBoxDirectoryName.CheckState = CheckState.Checked;
                else
                    checkBoxDirectoryName.CheckState = CheckState.Unchecked;

                if (checkNotifyFilters(SNF.getNotifyFilters(), NotifyFilters.FileName))
                    checkBoxFileName.CheckState = CheckState.Checked;
                else
                    checkBoxFileName.CheckState = CheckState.Unchecked;

                if (checkNotifyFilters(SNF.getNotifyFilters(), NotifyFilters.LastAccess))
                    checkBoxLastAccess.CheckState = CheckState.Checked;
                else
                    checkBoxLastAccess.CheckState = CheckState.Unchecked;

                if (checkNotifyFilters(SNF.getNotifyFilters(), NotifyFilters.LastWrite))
                    checkBoxLastWrite.CheckState = CheckState.Checked;
                else
                    checkBoxLastWrite.CheckState = CheckState.Unchecked;

                if (checkNotifyFilters(SNF.getNotifyFilters(), NotifyFilters.Security))
                    checkBoxSecurity.CheckState = CheckState.Checked;
                else
                    checkBoxSecurity.CheckState = CheckState.Unchecked;

                if (checkNotifyFilters(SNF.getNotifyFilters(), NotifyFilters.Size))
                    checkBoxSize.CheckState = CheckState.Checked;
                else
                    checkBoxSize.CheckState = CheckState.Unchecked;


                //Load Extens

                SettingExtensionFilters SEF = myObj.setting.SettingExtensionFilters;
                List<string> GEF = SEF.getExtensionFilters();
                listView4.Items.Clear();
                foreach (string item in GEF)
                {
                    ListViewItem list = new ListViewItem();
                    list.Text = item;
                    listView4.Items.Add(list);
                }
                //try
               // {
                    //Load Path
                    listView3.Items.Clear();
                    SettingPathLocation SPL = myObj.setting.SettingPathLocation;
                    List<String> ListPath = SPL.getListPath();
                    
                    foreach (string item in ListPath)
                    {
                        ListViewItem list = new ListViewItem();
                        list.Text = item;
                        listView3.Items.Add(list);
                    }
                //}
               // catch (Exception ex)
               // {
                //    Console.WriteLine(ex.Message);
                //}

               // bool GIS = SPL.getIncludeSubdirectories();

               // ListViewItem list3 = new ListViewItem();
               // list3.Text = GPL;
               // listView3.Items.Add(list3);




                //listView4.Items.Add(new ListViewItem().Text = GPL);

            }
            else
            {
                return;
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                SettingNotifyFilters SNF = new SettingNotifyFilters();
                if (checkBoxAttributes.CheckState == CheckState.Checked)
                    SNF.addNotifyFilters(NotifyFilters.Attributes);
                if (checkBoxCreationTime.CheckState == CheckState.Checked)
                    SNF.addNotifyFilters(NotifyFilters.CreationTime);
                if (checkBoxDirectoryName.CheckState == CheckState.Checked)
                    SNF.addNotifyFilters(NotifyFilters.DirectoryName);
                if (checkBoxFileName.CheckState == CheckState.Checked)
                    SNF.addNotifyFilters(NotifyFilters.FileName);
                if (checkBoxLastAccess.CheckState == CheckState.Checked)
                    SNF.addNotifyFilters(NotifyFilters.LastAccess);
                if (checkBoxLastWrite.CheckState == CheckState.Checked)
                    SNF.addNotifyFilters(NotifyFilters.LastWrite);
                if (checkBoxSecurity.CheckState == CheckState.Checked)
                    SNF.addNotifyFilters(NotifyFilters.Security);
                if (checkBoxSize.CheckState == CheckState.Checked)
                    SNF.addNotifyFilters(NotifyFilters.Size);


                //extens
                SettingExtensionFilters SEF = new SettingExtensionFilters();
                foreach (var str in listView4.Items)
                {
                    ListViewItem list = (ListViewItem)str;
                    SEF.addExtensionFilters(list.Text);
                }


                SettingUserEntity SUE = new SettingUserEntity(SEF, SNF, new SettingPathLocation());


                MonitoringClientInstance.SetAllSetting(ConnectService.Password, SUE);
            }
            catch
            {
                MessageBox.Show("Utracono polaczenie z serwerem!");
            }


        }

        private void button4_Click(object sender, EventArgs e)
        {
            ListViewItem list = new ListViewItem();
            list.Text = textBox2.Text;
            listView4.Items.Add(list);
            textBox2.Text = "";

        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (listView4.SelectedItems.Count > 0)
            {
                listView4.SelectedItems[0].Remove();
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            //zobacz plik
            try
            {
                if (LastReadSimilarHash != null)
                {
                    FileEntity fe = MonitoringClientInstance.getFileContent(LastReadSimilarHash);
                    if (fe != null)
                    {
                       NotepadHelper.ShowMessage(fe.FileContent, fe.ComputerName +":"+fe.nameFile);
                    }
                    else
                    {
                          MessageBox.Show("Nie zaznaczono wystapienia");
                    }
                }

            }
            catch (Exception se)
            {
                MessageBox.Show(se.Message);
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            tabPage2_Enter(null, null);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            try
            {
                ClearAllTab();
                var Similar = MonitoringClientInstance.GetSimilar(ConnectService.Password);
                FileEntity asd = null;
                for (int i = 0; i < Similar.Count; i ++)
                {
                    if (Similar[i] == null)
                    {
                        asd = Similar[i+1];
                        i++;
                        continue;
                    }
                    if (!(asd.ComputerName.Equals(Similar[i].ComputerName)))
                    
                    {
                        ListViewItem item = new ListViewItem();
                        item.Text = asd.UserName;
                        item.SubItems.Add(asd.ComputerName);
                        item.SubItems.Add(asd.nameFile);
                        item.SubItems.Add(asd.FileTime.ToString());


                        item.SubItems.Add(Similar[i].UserName);
                        item.SubItems.Add(Similar[i].ComputerName);
                        item.SubItems.Add(Similar[i].nameFile);
                        item.SubItems.Add(Similar[i].FileTime.ToString());

                        item.Tag = Similar[i].hashFile;
                        listView2.Items.Add(item);

                    }
                }
            }
            catch
            {
                MessageBox.Show("Utracono polaczenie z serwerem!");
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            textBox1.Text = comboBox1.Text;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            for (int i = listView3.Items.Count - 1; i >= 0; i--)
            {
                if (listView3.Items[i].Selected)
                {
                    listView3.Items[i].Remove();
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Equals(""))
            {
                MessageBox.Show("Puste pole");
            }
            else
            {
                 ListViewItem item = new ListViewItem();
                 item.Text = textBox1.Text;
                        listView3.Items.Add(item);

                
            }
        }

    }
}
