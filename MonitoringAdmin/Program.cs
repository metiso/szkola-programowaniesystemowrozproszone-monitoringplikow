﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
//using MonitoringAdmin.ServiceReference1;
using MonitoringSerwerWCF;
namespace MonitoringAdmin
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            try
            {
                IMonitoring mc = ConnectService.getConnectWCF();
                passwordBox pb = new passwordBox();
                string st = pb.Show("Podaj hasło do programu", "Hasło");
                ConnectService.Password = st.ToUpper();
                if (mc.checkPasswordProgram(st.ToUpper()))
                {
                    Application.EnableVisualStyles();
                    //Application.SetCompatibleTextRenderingDefault(false);
                    Application.Run(new Form1());
                }
                else
                {
                    MessageBox.Show("Złe hasło Admina !");
                    System.Environment.Exit(1);
                }
            }
            catch
            {
                MessageBox.Show("Brak polaczenia z sererem - sprawdz pozniej!");
            }
        }
    }
}
