﻿using Share;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;


namespace MonitoringSerwerWCF
{
    [ServiceContract(Namespace = "http://achilles.tu.kielce.pl")]
    public interface IMonitoring
    {
        [OperationContract]
        FileEntity getFileContent(String ArgHashFile);
        [OperationContract]
        bool checkConnect();
        [OperationContract]
        bool AddFile(FileEntity ArgFileEntity, SettingUserEntity ArgSue);
        [OperationContract]
        List<UserEntity> GetUser(String ArgHash);
        [OperationContract]
        void SetSetting(string ArgHash, UserEntity User);
         [OperationContract]
        void UpdateFile(FileEntity ArgFileEntity);
        [OperationContract]
        void SetAllSetting(string ArgHash, SettingUserEntity ArgSUE);
        [OperationContract]
        List<FileEntity> GetAllFile(String ArgHash);
        [OperationContract]
        List<FileEntity> GetSimilar(String ArgHash);
        [OperationContract]
        void AddUser(FileEntity ArgFileEntity, SettingUserEntity ArgSue);
        [OperationContract]
        SettingUserEntity GetSettingUser(String ArgUserName, String ArgComputerName);
        [OperationContract]
        bool checkPasswordProgram(String ArgHash);
        [OperationContract]
        void setPasswordProgram(String ArgHash);
        [OperationContract]
        string GetData(int value);
    }
}
