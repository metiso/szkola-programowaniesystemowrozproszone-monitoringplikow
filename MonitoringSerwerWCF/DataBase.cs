﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;
//using System.Data.SqlServerCe;


namespace MonitoringSerwerWCF
{
    public class DataBase
    {


        /*
         * CREATE TABLE dbo.usersMonitor(
 UserName VARCHAR(255) NULL, 
 ComputerName VARCHAR(255) NULL,
 setting VARBINARY(MAX) NULL);

 CREATE TABLE dbo.FileMonitor(
 nameFile VARCHAR(255) NULL, 
 hashFile VARCHAR(255) NULL, 
 FileInfo VARBINARY(MAX) NULL, 
 UserName VARCHAR(255) NULL,
 ComputerName VARCHAR(255) NULL,
 Date DateTime NULL);


 * */
        #region Variables
public static String connectionString = @"Data Source =DESKTOP-7K2GG2M;Initial Catalog=FIRMA_ASP;Integrated Security=True";
        private SqlConnection conString;

        #endregion

        #region Builders and Finalizers

        public DataBase()
        {
            this.conString = new SqlConnection(DataBase.connectionString);
        }

        public DataBase(string conStr)
        {
            this.conString = new SqlConnection(conStr);
        }

        #endregion

        #region Properties

        public SqlConnection ConnectionString
        {
            get
            {
                return this.conString;
            }
            set
            {
                this.conString = value;
            }
        }

        #endregion

        #region Methods

        public void Connect()
        {
            try
            {
                this.conString.Open();
            }
            catch (InvalidCastException ex)
            {
                throw ex;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        public void Disconnect()
        {
            try
            {
                this.conString.Close();
            }
            catch
            {

            }
        }

        public SqlDataAdapter CreateAdapter(string query)
        {
            return new SqlDataAdapter(query, this.ConnectionString);
        }

        public DataTable CreateDataTable(string query)
        {
            DataTable tab = new DataTable();

            this.Connect();

            if (this.ConnectionString.State == ConnectionState.Open)
            {
                SqlDataAdapter ad = CreateAdapter(query);

                ad.Fill(tab);
            }
            else
            {
                throw new Exception("Nie można połączyć się z bazą daych");
            }

            this.Disconnect();

            return tab;
        }

        public DataRow CreateDataRow(string query)
        {
            DataTable tab = new DataTable();

            DataRow row;

            this.Connect();

            if (this.ConnectionString.State == ConnectionState.Open)
            {

                SqlDataAdapter ad = CreateAdapter(query);

                ad.Fill(tab);
                try
                {
                    row = tab.Rows[0];
                }
                catch
                {
                    row = null;
                }
            }
            else
            {
                throw new Exception("Nie można połączyć się z bazą daych");
            }

            this.Disconnect();

            return row;
        }

        public SqlConnection getConnectionString()
        {
            return this.ConnectionString;
        }
        public SqlDataReader RunQueryResult(SqlCommand cmdQuery)
        {
            this.Connect();

            SqlDataReader ret = null;
            if (this.ConnectionString.State == ConnectionState.Open)
            {
                
                SqlCommand cmd = cmdQuery;
                cmd.Connection = this.ConnectionString;
                ret = cmd.ExecuteReader();
               
                
            }
            else
            {
               
              throw new Exception("Nie można połączyć się z bazą daych");
            }
            this.Disconnect();
            if (ret == null)
                return null;

            return ret;
        }
        public void RunQuery(SqlCommand cmdQuery)
        {
            this.Connect();

            if (this.ConnectionString.State == ConnectionState.Open)
            {
                SqlCommand cmd = cmdQuery;
                cmd.Connection = this.ConnectionString;

                cmd.ExecuteNonQuery();
            }
            else
            {
                throw new Exception("Nie można połączyć się z bazą daych");
            }

            this.Disconnect();
        }
        public void RunQuery(string query)
        {
            this.Connect();

            if (this.ConnectionString.State == ConnectionState.Open)
            {
                SqlCommand cmd = new SqlCommand(query, this.ConnectionString);

                cmd.ExecuteNonQuery();
            }
            else
            {
                throw new Exception("Nie można połączyć się z bazą daych");
            }

            this.Disconnect();
        }
    
        #endregion
    }
}
