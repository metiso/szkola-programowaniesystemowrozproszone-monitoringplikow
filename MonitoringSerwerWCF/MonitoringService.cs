﻿//using MonitoringKlient;
using Share;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.ServiceModel;
using System.Text;
using System.Threading;

namespace MonitoringSerwerWCF
{

    public class MonitoringService : IMonitoring
    {
        string hashFunc = "E4F5691E6481FFC3DD048042DEDA5C6F"; // monitor1
        static List<FileEntity> FileUser = new List<FileEntity>();
        static List<UserEntity> Users = new List<UserEntity>();


        private static DataBase dbo = null;

        public static DataBase DataBaseInstance
        {
            get
            {
                if (dbo == null)
                {
                    dbo = new DataBase();
                    //  dbo.Connect();
                }
                return dbo;
            }
        }



        // DataBase db;
        public void AddUser(FileEntity ArgFileEntity, SettingUserEntity ArgSue)
        {
            SqlCommand sqlCmd = new SqlCommand("SELECT count(*) as LICZNIK FROM usersMonitor  WHERE UserName=@UserName AND ComputerName=@ComputerName ");
            if (String.IsNullOrEmpty(ArgFileEntity.UserName))
                sqlCmd.Parameters.AddWithValue("@UserName", DBNull.Value);
            else
                sqlCmd.Parameters.AddWithValue("@UserName", ArgFileEntity.UserName);

            if (String.IsNullOrEmpty(ArgFileEntity.ComputerName))
                sqlCmd.Parameters.AddWithValue("@ComputerName", DBNull.Value);
            else
                sqlCmd.Parameters.AddWithValue("@ComputerName", ArgFileEntity.ComputerName);



            int count = 0;

            //
            MonitoringService.DataBaseInstance.Connect();
            SqlDataReader ret = null;
            if (MonitoringService.DataBaseInstance.getConnectionString().State == ConnectionState.Open)
            {
                sqlCmd.Connection = MonitoringService.DataBaseInstance.getConnectionString();
                ret = sqlCmd.ExecuteReader();
                if (ret.Read())
                {
                    string column = ret["LICZNIK"].ToString();
                    int columnValue = Convert.ToInt32(ret["LICZNIK"]);
                    count = columnValue;
                }
            }
            else
            {
                throw new Exception("Nie można połączyć się z bazą daych");
            }
            MonitoringService.DataBaseInstance.Disconnect();

            if (count < 1)
            {
                //Users.Add(new UserEntity(ArgFileEntity.UserName, ArgFileEntity.ComputerName, ArgSue));


                //SELECT TOP (1000) [UserName]  ,[ComputerName] ,[setting] FROM [FIRMA_ASP].[dbo].[usersMonitor]

                sqlCmd = new SqlCommand("INSERT INTO usersMonitor(UserName,ComputerName,setting) VALUES (@UserName,@ComputerName,@setting)");

                if (String.IsNullOrEmpty(ArgFileEntity.UserName))
                    sqlCmd.Parameters.AddWithValue("@UserName", DBNull.Value);
                else
                    sqlCmd.Parameters.AddWithValue("@UserName", ArgFileEntity.UserName);

                if (String.IsNullOrEmpty(ArgFileEntity.ComputerName))
                    sqlCmd.Parameters.AddWithValue("@ComputerName", DBNull.Value);
                else
                    sqlCmd.Parameters.AddWithValue("@ComputerName", ArgFileEntity.ComputerName);


                MemoryStream memoryStream = new MemoryStream();
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(memoryStream, ArgSue);

                sqlCmd.Parameters.Add("@setting", SqlDbType.VarBinary, Int32.MaxValue).Value = memoryStream.ToArray();

                MonitoringService.DataBaseInstance.RunQuery(sqlCmd);
                memoryStream.Close();


            }

        }

        public bool AddFile(FileEntity ArgFileEntity, SettingUserEntity ArgSue)
        {
            
            //sprawdz czy istnieje taki sam jesli tak to wyslji true jesli nie to false
            SqlCommand sqlCmd = new SqlCommand("SELECT count(*) as LICZNIK FROM FileMonitor WHERE UserName=@UserName AND ComputerName=@ComputerName AND hashFile=@hashFile  ");
            if (String.IsNullOrEmpty(ArgFileEntity.nameFile))
                sqlCmd.Parameters.AddWithValue("@UserName", DBNull.Value);
            else
                sqlCmd.Parameters.AddWithValue("@UserName", ArgFileEntity.UserName);

            if (String.IsNullOrEmpty(ArgFileEntity.ComputerName))
                sqlCmd.Parameters.AddWithValue("@ComputerName", DBNull.Value);
            else
                sqlCmd.Parameters.AddWithValue("@ComputerName", ArgFileEntity.ComputerName);

            if (String.IsNullOrEmpty(ArgFileEntity.hashFile))
                sqlCmd.Parameters.AddWithValue("@hashFile", DBNull.Value);
            else
                sqlCmd.Parameters.AddWithValue("@hashFile", ArgFileEntity.hashFile);

            int count = 0;

            //
            MonitoringService.DataBaseInstance.Connect();
            SqlDataReader ret = null;
            if (MonitoringService.DataBaseInstance.getConnectionString().State == ConnectionState.Open)
            {

                sqlCmd.Connection = MonitoringService.DataBaseInstance.getConnectionString();
                ret = sqlCmd.ExecuteReader();

                if (ret.Read())
                {
                    string column = ret["LICZNIK"].ToString();
                    int columnValue = Convert.ToInt32(ret["LICZNIK"]);
                    count = columnValue;
                    Console.WriteLine(count);
                }


            }
            else
            {
                throw new Exception("Nie można połączyć się z bazą daych");
            }
            MonitoringService.DataBaseInstance.Disconnect();

            Console.WriteLine(count + "<1");
            if (count < 1)
            {
                sqlCmd = new SqlCommand("INSERT INTO FileMonitor(nameFile,hashFile,UserName,ComputerName,Date) VALUES (@nameFile,@hashFile,@UserName,@ComputerName,@Date)");

                if (String.IsNullOrEmpty(ArgFileEntity.nameFile))
                    sqlCmd.Parameters.AddWithValue("@nameFile", DBNull.Value);
                else
                    sqlCmd.Parameters.AddWithValue("@nameFile", ArgFileEntity.nameFile);


                if (String.IsNullOrEmpty(ArgFileEntity.hashFile))
                    sqlCmd.Parameters.AddWithValue("@hashFile", DBNull.Value);
                else
                    sqlCmd.Parameters.AddWithValue("@hashFile", ArgFileEntity.hashFile);


                if (String.IsNullOrEmpty(ArgFileEntity.UserName))
                    sqlCmd.Parameters.AddWithValue("@UserName", DBNull.Value);
                else
                    sqlCmd.Parameters.AddWithValue("@UserName", ArgFileEntity.UserName);


                if (String.IsNullOrEmpty(ArgFileEntity.ComputerName))
                    sqlCmd.Parameters.AddWithValue("@ComputerName", DBNull.Value);
                else
                    sqlCmd.Parameters.AddWithValue("@ComputerName", ArgFileEntity.ComputerName);

                sqlCmd.Parameters.AddWithValue("@Date", ArgFileEntity.FileTime.ToString("yyyy-MM-dd HH:mm:ss"));

                MonitoringService.DataBaseInstance.RunQuery(sqlCmd);

                // AddUser(ArgFileEntity, ArgSue);



                //sprawdz czy istnieje taki sam jesli tak to wyslji true jesli nie to false
                sqlCmd = new SqlCommand("SELECT count(*) as LICZNIK FROM FileMonitor WHERE hashFile=@hashFile ");
     
                if (String.IsNullOrEmpty(ArgFileEntity.hashFile))
                    sqlCmd.Parameters.AddWithValue("@hashFile", DBNull.Value);
                else
                    sqlCmd.Parameters.AddWithValue("@hashFile", ArgFileEntity.hashFile);



                 count = 0;

                //
                MonitoringService.DataBaseInstance.Connect();
                ret = null;
                if (MonitoringService.DataBaseInstance.getConnectionString().State == ConnectionState.Open)
                {

                    sqlCmd.Connection = MonitoringService.DataBaseInstance.getConnectionString();
                    ret = sqlCmd.ExecuteReader();

                    if (ret.Read())
                    {
                        string column = ret["LICZNIK"].ToString();
                        int columnValue = Convert.ToInt32(ret["LICZNIK"]);
                        count = columnValue;
                        //Console.WriteLine(count);
                    }


                }
                else
                {
                    throw new Exception("Nie można połączyć się z bazą daych");
                }
                MonitoringService.DataBaseInstance.Disconnect();


                if (count > 1)
                {
                    return true;
                }
                return false;
                
            }
           return false;
        }

        public FileEntity getFileContent(String ArgHashFile)
        {
            FileEntity FEReturn = null;
            try
            {

                SqlCommand sqlCmd = new SqlCommand("SELECT TOP (1) [nameFile]  ,[hashFile] ,[UserName]  ,[ComputerName]  ,[Date],[Filecontent] FROM FileMonitor WHERE hashFile=@hashFile ");

                if (String.IsNullOrEmpty(ArgHashFile))
                    sqlCmd.Parameters.AddWithValue("@hashFile", DBNull.Value);
                else
                    sqlCmd.Parameters.AddWithValue("@hashFile", ArgHashFile);

                
                MonitoringService.DataBaseInstance.Connect();
                SqlDataReader ret = null;
                if (MonitoringService.DataBaseInstance.getConnectionString().State == ConnectionState.Open)
                {
                    sqlCmd.Connection = MonitoringService.DataBaseInstance.getConnectionString();
                    ret = sqlCmd.ExecuteReader();

                    if (ret.Read())
                    {
                        FEReturn = new FileEntity();
                        FEReturn.nameFile = ret["nameFile"].ToString();
                        FEReturn.UserName = ret["UserName"].ToString();
                        FEReturn.ComputerName = ret["ComputerName"].ToString();
                        MemoryStream ms2 = new MemoryStream();
                        byte[] buf = (byte[])ret["Filecontent"];
                        ms2.Write(buf, 0, buf.Length);
                        ms2.Seek(0, 0);
                        BinaryFormatter b = new BinaryFormatter();
                        FEReturn.FileContent = (string)b.Deserialize(ms2);
                    }
                    ret.Close();


                }
                else
                {
                    throw new Exception("Nie można połączyć się z bazą daych");
                }
                MonitoringService.DataBaseInstance.Disconnect();
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
            return FEReturn;
        }
        public void UpdateFile(FileEntity ArgFileEntity)
        {

            SqlCommand sqlCmd = new SqlCommand("UPDATE FileMonitor SET Filecontent=@Filecontent WHERE hashFile=@hashFile");

            if (String.IsNullOrEmpty(ArgFileEntity.hashFile))
                sqlCmd.Parameters.AddWithValue("@hashFile", DBNull.Value);
            else
                sqlCmd.Parameters.AddWithValue("@hashFile", ArgFileEntity.hashFile);



            MemoryStream memoryStream = new MemoryStream();
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(memoryStream, ArgFileEntity.FileContent);

            sqlCmd.Parameters.Add("@Filecontent", SqlDbType.VarBinary, Int32.MaxValue).Value = memoryStream.ToArray();

            MonitoringService.DataBaseInstance.RunQuery(sqlCmd);
            memoryStream.Close();

        }
        public string GetData(int value)
        {
            var result = string.Format("You entered \"{0}\" via \"{1}\"",
                value,
                OperationContext.Current.RequestContext.RequestMessage.Headers.To);

            Console.WriteLine(result);
            return result;
        }

        public List<FileEntity> GetSimilar(String ArgHash)
        {
            List<FileEntity> FilesList = new List<FileEntity>();


            List<String> hashList = new List<String>();
            int columnValue = 0;
            string columnHash = "";
            SqlCommand sqlCmd = new SqlCommand("SELECT hashFile FROM FileMonitor GROUP BY hashFile HAVING COUNT(*) > 1");
            MonitoringService.DataBaseInstance.Connect();
            SqlDataReader ret = null;
            if (MonitoringService.DataBaseInstance.getConnectionString().State == ConnectionState.Open)
            {
                sqlCmd.Connection = MonitoringService.DataBaseInstance.getConnectionString();
                ret = sqlCmd.ExecuteReader();
                while (ret.Read())
                {
                    hashList.Add(ret["hashFile"].ToString());
                }
                ret.Close();
            }

            MonitoringService.DataBaseInstance.Disconnect();

            //string hasho = hashList[0];
            MonitoringService.DataBaseInstance.Connect();
            SqlDataReader reto = null;
            try
            {
                if (MonitoringService.DataBaseInstance.getConnectionString().State == ConnectionState.Open)
                {

                    foreach (string hash in hashList)
                    {
                        SqlCommand sqlCmdo = new SqlCommand("SELECT nameFile,UserName,ComputerName,Date,hashFile FROM FileMonitor WHERE hashFile=@hashFile");
                        sqlCmdo.Parameters.AddWithValue("@hashFile", hash);
                        sqlCmdo.Connection = MonitoringService.DataBaseInstance.getConnectionString();
                        reto = sqlCmdo.ExecuteReader();
                        // int count = 0;
                        FileEntity lastFileEntity = null;
                        FileEntity FE = null;
                        FilesList.Add(null);
                        while (reto.Read())
                        {
                            //count++;
                            lastFileEntity = FE;
                            FE = new FileEntity(reto["nameFile"].ToString(), reto["hashFile"].ToString(), DateTime.ParseExact(reto["Date"].ToString(), "dd.MM.yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture), reto["UserName"].ToString(), reto["ComputerName"].ToString());
                            FilesList.Add(FE);

                        }]
                        reto.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            MonitoringService.DataBaseInstance.Disconnect();

]

            return FilesList;
        }
        public List<UserEntity> GetUser(String ArgHash)
        {
            List<UserEntity> Userss = new List<UserEntity>();
            MonitoringService.DataBaseInstance.Connect();
            SqlDataReader ret = null;
            if (MonitoringService.DataBaseInstance.getConnectionString().State == ConnectionState.Open)
            {
                SqlCommand sqlCmd = new SqlCommand("SELECT UserName,ComputerName,setting FROM usersMonitor  ");

                sqlCmd.Connection = MonitoringService.DataBaseInstance.getConnectionString();
                ret = sqlCmd.ExecuteReader();

                BinaryFormatter binaryFormatter = new BinaryFormatter();
                while (ret.Read())
                {

                    MemoryStream ms2 = new MemoryStream();
                    byte[] buf = (byte[])ret["setting"];
                    ms2.Write(buf, 0, buf.Length);
                    ms2.Seek(0, 0);
                    BinaryFormatter b = new BinaryFormatter();
                    SettingUserEntity deserializedYourObject = (SettingUserEntity)b.Deserialize(ms2);
                    Userss.Add(new UserEntity(ret["UserName"].ToString(), ret["ComputerName"].ToString(), deserializedYourObject));
                }
            }
            else
            {
                throw new Exception("Nie można połączyć się z bazą daych");
            }
            MonitoringService.DataBaseInstance.Disconnect();

          if (checkPassword(ArgHash))
                return Userss;
            else
                return null;
        }

        public SettingUserEntity GetSettingUser(String ArgUserName, String ArgComputerName)
        {
            SettingUserEntity returnSUE = new SettingUserEntity(new SettingExtensionFilters(), new SettingNotifyFilters(), new SettingPathLocation());

            try
            {
                Console.Write("1");
                MonitoringService.DataBaseInstance.Connect();
                SqlDataReader ret = null;
                if (MonitoringService.DataBaseInstance.getConnectionString().State == ConnectionState.Open)
                {
                    Console.Write("2");
                    SqlCommand sqlCmd = new SqlCommand("SELECT setting FROM usersMonitor WHERE UserName=@UserName AND ComputerName=@ComputerName ");
                    if (String.IsNullOrEmpty(ArgUserName))
                        sqlCmd.Parameters.AddWithValue("@UserName", DBNull.Value);
                    else
                        sqlCmd.Parameters.AddWithValue("@UserName", ArgUserName);

                    if (String.IsNullOrEmpty(ArgComputerName))
                        sqlCmd.Parameters.AddWithValue("@ComputerName", DBNull.Value);
                    else
                        sqlCmd.Parameters.AddWithValue("@ComputerName", ArgComputerName);

                    sqlCmd.Connection = MonitoringService.DataBaseInstance.getConnectionString();
                    ret = sqlCmd.ExecuteReader();

                    BinaryFormatter binaryFormatter = new BinaryFormatter();
                    while (ret.Read())
                    {
                        Console.Write("3");
                        MemoryStream ms2 = new MemoryStream();
                        byte[] buf = (byte[])ret["setting"];
                        ms2.Write(buf, 0, buf.Length);
                        ms2.Seek(0, 0);
                        BinaryFormatter b = new BinaryFormatter();
                        SettingUserEntity deserializedYourObject = (SettingUserEntity)b.Deserialize(ms2);
                        // Userss.Add(new UserEntity(ret["UserName"].ToString(), ret["ComputerName"].ToString(), deserializedYourObject));
                        returnSUE = deserializedYourObject;
                    }
                    ret.Close();
                }
                else
                {
                    Thread.Sleep(1500);
                    GetSettingUser(ArgUserName, ArgComputerName);
                    Console.Write("4");
                    //throw new Exception("Nie można połączyć się z bazą daych");

                }
                MonitoringService.DataBaseInstance.Disconnect();
            }
            catch (Exception e)
            {

                Thread.Sleep(1500);
                GetSettingUser(ArgUserName, ArgComputerName);
                //  Console.WriteLine(e.Message);
            }
            // Console.Write("5");
            // Console.Write(returnSUE.SettingExtensionFilters.getExtensionFilters()[0]);
            return returnSUE;
            ////SettingExtensionFilters test1 = new SettingExtensionFilters();
            //test1.addExtensionFilters(".txt");
            //  Users.Add(new UserEntity("imie1", "komputer2", new SettingUserEntity(test1, new SettingNotifyFilters(), new SettingPathLocation())));
            // Users.Add(new UserEntity("imie2", "kkomputer2", new SettingUserEntity(test1, new SettingNotifyFilters(), new SettingPathLocation())));


            //GetSettingUser(ArgFileEntity.UserName, ArgFileEntity.ComputerName);

        }
        public void SetSetting(string ArgHash, UserEntity User)
        {
            if (checkPassword(ArgHash))
            {

                SqlCommand sqlCmd = new SqlCommand("UPDATE usersMonitor SET setting=@setting WHERE UserName=@UserName AND ComputerName=@ComputerName ");
                if (String.IsNullOrEmpty(User.UserName))
                    sqlCmd.Parameters.AddWithValue("@UserName", DBNull.Value);
                else
                    sqlCmd.Parameters.AddWithValue("@UserName", User.UserName);

                if (String.IsNullOrEmpty(User.ComputerName))
                    sqlCmd.Parameters.AddWithValue("@ComputerName", DBNull.Value);
                else
                    sqlCmd.Parameters.AddWithValue("@ComputerName", User.ComputerName);



                MemoryStream memoryStream = new MemoryStream();
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(memoryStream, User.setting);

                sqlCmd.Parameters.Add("@setting", SqlDbType.VarBinary, Int32.MaxValue).Value = memoryStream.ToArray();

                MonitoringService.DataBaseInstance.RunQuery(sqlCmd);
                memoryStream.Close();



            }

        }
        public void SetAllSetting(string ArgHash, SettingUserEntity ArgSUE)
        {
            if (checkPassword(ArgHash))
            {


                SqlCommand sqlCmd = new SqlCommand("UPDATE usersMonitor SET setting=@setting");


                MemoryStream memoryStream = new MemoryStream();
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(memoryStream, ArgSUE);

                sqlCmd.Parameters.Add("@setting", SqlDbType.VarBinary, Int32.MaxValue).Value = memoryStream.ToArray();

                MonitoringService.DataBaseInstance.RunQuery(sqlCmd);
                memoryStream.Close();

            }

        }
        public List<FileEntity> GetAllFile(String ArgHash)
        {
            if (checkPassword(ArgHash))
                return FileUser;
            else
                return null;
        }
        public bool checkPasswordProgram(String ArgHash)
        {
            if (checkPassword(ArgHash))
                return true;
            else
                return false;
        }
        public void setPasswordProgram(String ArgHash)
        {
            if (checkPassword(ArgHash))
            {
                hashFunc = ArgHash;
            }
        }
        private bool checkPassword(String ArgHash)
        {
            // return true;
            if (ArgHash.Equals(hashFunc))
                return true;
            else
                return false;
        }

        public bool checkConnect()
        {
            return true;
        }
    }
}
