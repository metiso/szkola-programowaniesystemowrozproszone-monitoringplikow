Zadaniem było opracowanie aplikacji do monitorowania plików w danym katalogu. 
Jeśli dwa identyczne pliki pojawią się na komputerze ma być on wyświetlony na aplikacji administracyjnej.

Aplikacja WCF usadzona na serwerze działa w tle i oczekuje na połączenie klientów. 
Metody administracyjne wymagają do uwierzytelnienia hasła dostępu.
Klient gdy tylko wykryje zmianę w folderze Monitorowanym jakiegoś pliku automatycznie wysyła jego ‘hash” i podstawowe informacje pliku na serwer który on zostaje zapisany w bazie. 

Jeśli jego ‘hash’ się powtórzył wysyła zawartość pliku aby później administrator mógł go zobaczyć. 
Przy pierwszym uruchomieniu klienta klient wysyła swoją nazwę komputera oraz nazwę użytkownika na serwer aby można było indywidualnie ustawić ustawienia.

Screen z działania programu : (tylko aplikacji adminstracyjnej ponieważ Aplikacja Kliencka działa tle jako usługa systemu windows)
![](Screen/1.png)
![](Screen/2.png)

Założenia Projektu : 
* -Osobne aplikacje dla Administratora, Serwera oraz klienta 
* -Monitorowanie plików z wybranych katalogów 
* -Monitorowanie wybranych rozszerzeń plików 
* -Ustawienia plików do monitorowania 
* -Kolejkowanie plików gdy serwer niedostępny 
* -Wysyłanie zawartości pliku jeśli się powtórzy na innym PC 
* -Wgląd na identyczne pliki (Podgląd pliku) 
* -Wyświetlenie daty pliku oraz nazwy użytkownika i komputera
* -Odbieranie przez klienta aktualnych ustawien 
