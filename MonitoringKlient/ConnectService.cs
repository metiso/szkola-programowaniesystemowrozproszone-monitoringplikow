﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Discovery;
//using DiscaveryClientApp.MonitoringServiceReference1;
using System.Collections.ObjectModel;
using System.IO;
using MonitoringSerwerWCF;
using Share;
using System.Xml;
using System.Xml.Serialization;
//using MonitoringKlient.ServiceReference1;

namespace MonitoringKlient
{

    class ConnectService
    {
        public static int i = 0;
        public static SettingUserEntity SUESetting = null;
        public static string Password { get; set; }

        static IMonitoring proxy = null;
        private static IMonitoring ConnectWCF()
        {
            if (proxy == null)
            {
                var client = new DiscoveryClient(new UdpDiscoveryEndpoint());
                var criteria = new FindCriteria(typeof(IMonitoring));
                criteria.MaxResults = 1;
                criteria.Duration = TimeSpan.FromSeconds(5);

                var findResult = client.Find(criteria);


                // Console.WriteLine();
                var address = findResult.Endpoints.First(ep => ep.Address.Uri.Scheme == "http").Address;
                Console.WriteLine(address);
                var factory = new ChannelFactory<IMonitoring>(new BasicHttpBinding(), address);
                proxy = factory.CreateChannel();
                //Console.WriteLine("")
                return proxy;
            }
            return proxy;

        }

        public static void closeConnectWCF()
        {
            //Console.WriteLine(ConnectService.getConnectWCF().checkPasswordProgram("").ToString());
            // ((ICommunicationObject)proxy.Close();
        }

        public static IMonitoring getConnectWCF()
        {
            return ConnectWCF();
        }
        public static void checkSetting()
        {
            try
            {
                SUESetting = proxy.GetSettingUser(Environment.UserName, Environment.MachineName);
                //zapisz ustawienia do pliku

                proxy.AddUser(new FileEntity(null, null, new DateTime(), Environment.UserName, Environment.MachineName), new SettingUserEntity(new SettingExtensionFilters(), new SettingNotifyFilters(), new SettingPathLocation()));

                WriteToBinaryFile<SettingUserEntity>("mtr.bat", SUESetting);
                proxy.checkConnect();
                int x = FileWatcher.QueueSendToSerwer.Count();
                for (int i = 0; i < x; i++)
                {

                    FileEntity tmpque = FileWatcher.QueueSendToSerwer.Dequeue();
                   // Console.WriteLine("RE:" + tmpque.nameFile);
                    string tmp = tmpque.FileContent;
                    tmpque.FileContent = null;
                    if (proxy.AddFile(tmpque, ConnectService.getSetting()))
                    {
                        FileEntity tmpFE = new FileEntity();
                        tmpFE.FileContent = tmp;
                        tmpFE.hashFile = tmpque.hashFile;
                        proxy.UpdateFile(tmpFE);
                    }
                }
            }
            catch
            {
               SUESetting = (SettingUserEntity)ReadFromBinaryFile<SettingUserEntity>("mtr.bat");
            }

        }

        public static SettingUserEntity getSetting()
        {
            if (SUESetting == null)
                checkSetting();

            return SUESetting;
        }

        /// <summary>
        /// Writes the given object instance to a binary file.
        /// <para>Object type (and all child types) must be decorated with the [Serializable] attribute.</para>
        /// <para>To prevent a variable from being serialized, decorate it with the [NonSerialized] attribute; cannot be applied to properties.</para>
        /// </summary>
        /// <typeparam name="T">The type of object being written to the binary file.</typeparam>
        /// <param name="filePath">The file path to write the object instance to.</param>
        /// <param name="objectToWrite">The object instance to write to the binary file.</param>
        /// <param name="append">If false the file will be overwritten if it already exists. If true the contents will be appended to the file.</param>
        public static void WriteToBinaryFile<T>(string filePath, T objectToWrite, bool append = false)
        {
            using (Stream stream = File.Open(filePath, append ? FileMode.Append : FileMode.Create))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                binaryFormatter.Serialize(stream, objectToWrite);
            }
        }

        /// <summary>
        /// Reads an object instance from a binary file.
        /// </summary>
        /// <typeparam name="T">The type of object to read from the binary file.</typeparam>
        /// <param name="filePath">The file path to read the object instance from.</param>
        /// <returns>Returns a new instance of the object read from the binary file.</returns>
        public static T ReadFromBinaryFile<T>(string filePath)
        {
            using (Stream stream = File.Open(filePath, FileMode.Open))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                return (T)binaryFormatter.Deserialize(stream);
            }
        }

    }
}