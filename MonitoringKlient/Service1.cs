﻿using MonitoringSerwerWCF;
using Share;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MonitoringKlient
{
    public partial class Service1 : ServiceBase
    {
        static List<FileWatcher> listWatcher = new List<FileWatcher>();
        FileWatcher f = null;
        public Service1()
        {
            InitializeComponent();
        }

        public void OnDebug(string[] args)
        {

            OnStart(null);
        }
        private static void TimerMetod()
        {
            while (true)
            {
                try
                {
                    IMonitoring mc = ConnectService.getConnectWCF(); //polaczenie z serwerem wcf
                    if (mc.checkConnect()) //sprawdzenie polaczenia
                    {
                        ConnectService.checkSetting(); //pobranie ustawien danego uzytwkonika
                        List<string> ListPath = ConnectService.getSetting().SettingPathLocation.getListPath(); 
                        //pobranie listy monitorowanych folderow

                        foreach (FileWatcher FW in listWatcher)
                            { FW._fileWatcher.EnableRaisingEvents = false;}
                        //wylacznie ogladania plikow

                        int count = 0;
                        for (int x = 0; x < listWatcher.Count; x++) //ustawienie patchy dla watcherow 
                        {
                            count++;
                            var Path = ListPath[x].Replace("%USERPROFILE%", Environment.GetFolderPath(Environment.SpecialFolder.UserProfile));
                            Path = Path.Replace("%USERDESKTOP%", Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
                            listWatcher[x]._fileWatcher.Path = Path;
                        }
                        for (int x = count; x < ListPath.Count - 1; x++) 
                        { //dodanie nowych watcherow jesli jest wiecej patchow
                            listWatcher.Add(new FileWatcher(ListPath[x]));
                        }
                        for (int i = 0; i < ListPath.Count; i++)
                        { //ustawienie filtrow oraz wlaczenie ogladania plikow
                            listWatcher[i]._fileWatcher.NotifyFilter = ConnectService.getSetting().SettingNotifyFilters.getNotifyFilters();
                            listWatcher[i]._fileWatcher.EnableRaisingEvents = true;
                        }
                    }
                    Thread.Sleep(5*1000); //occzekiwanie 5 sekund na pobranie nowych ustawien od serwera
                }
                catch (Exception es)
                {
                    Thread.Sleep(1000); //jesli wystawpi blad odczekaj 1sek i sproboj pobrac od nowa ustawienia
                }
            }
        }
        protected override void OnStart(string[] args)
        {
            IMonitoring mc = null;
                try
                {
                    mc = ConnectService.getConnectWCF();
                }
                catch { }

            FileWatcher f = new FileWatcher("%USERPROFILE%");
            listWatcher.Add(f);

            Thread t = new Thread(TimerMetod);
            t.Start();
            
        }

        protected override void OnStop()
        {
        }
    }
}
