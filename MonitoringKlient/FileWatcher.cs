﻿//using MonitoringKlient.ServiceReference1;
using MonitoringSerwerWCF;
using Share;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.ServiceModel.Discovery;
using System.Text;
using System.Threading.Tasks;


namespace MonitoringKlient
{
    public class FileWatcher
    {

        public static Queue<FileEntity> QueueSendToSerwer = new Queue<FileEntity>();
        //SettingUserEntity sue;
        bool let = false;
        public FileSystemWatcher _fileWatcher;

        public FileWatcher(string ArgPath)
        {
            //sue = 
            _fileWatcher = new FileSystemWatcher();
            //  SettingPathLocation SPL = new SettingPathLocation();
            //  SPL.setPathLocation(ArgSPL);
            // sue = new SettingUserEntity(new SettingExtensionFilters(), new SettingNotifyFilters(), SPL);


            //  Console.WriteLine(sue.SettingPathLocation.getPathLocation);
            Console.WriteLine(ConnectService.getSetting().SettingPathLocation.getPathLocation().Equals("%USERPROFILE%").ToString());
            Console.WriteLine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile));

            var Path = ArgPath.Replace("%USERPROFILE%", Environment.GetFolderPath(Environment.SpecialFolder.UserProfile));
             Path = Path.Replace("%USERDESKTOP%", Environment.GetFolderPath(Environment.SpecialFolder.Desktop));

           // if (ConnectService.getSetting().SettingPathLocation.getPathLocation().Equals("%USERPROFILE%"))
           // if (ArgPath.Equals("%USERPROFILE%"))
             Console.WriteLine("Path:"+Path);
             _fileWatcher.Path = Path;// Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);

            _fileWatcher.IncludeSubdirectories = ConnectService.getSetting().SettingPathLocation.getIncludeSubdirectories();


            //ConnectService.getSetting().SettingNotifyFilters.addNotifyFilters(NotifyFilters.FileName);
            //onnectService.getSetting().SettingNotifyFilters.addNotifyFilters(NotifyFilters.Size);
            //ConnectService.getSetting().SettingNotifyFilters.addNotifyFilters(NotifyFilters.LastWrite);
           // ConnectService.getSetting().SettingNotifyFilters.addNotifyFilters(NotifyFilters.Attributes);


            //_fileWatcher.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.Size | NotifyFilters.FileName;
            _fileWatcher.NotifyFilter = ConnectService.getSetting().SettingNotifyFilters.getNotifyFilters();

            _fileWatcher.Created += new FileSystemEventHandler(_fileWatcher_Changed);
            _fileWatcher.Deleted += new FileSystemEventHandler(_fileWatcher_Changed);
            _fileWatcher.Changed += new FileSystemEventHandler(_fileWatcher_Changed);
            _fileWatcher.Renamed += new RenamedEventHandler(_fileWatcher_Changed);
            _fileWatcher.Filter = "*.*";
            /*
            sue.SettingExtensionFilters.addExtensionFilters(".txt");
            sue.SettingExtensionFilters.addExtensionFilters(".rar");
            sue.SettingExtensionFilters.addExtensionFilters(".jpg");
            */

            _fileWatcher.EnableRaisingEvents = true; //Rozpoczyna oglądanie plików
        }

        public static string GetIPAddress()
        {
            IPHostEntry host;
            string localIP = "?";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                }
            }
            return localIP;
        }

        void _fileWatcher_Changed(object source, FileSystemEventArgs e)
        {
           // if (let == false)
           // {
                //let = true;
                //try
                //{
                Console.WriteLine("TAK");
                List<string> test = ConnectService.getSetting().SettingExtensionFilters.getExtensionFilters();
                foreach(string asd in test)
                {
                    Console.WriteLine(asd);
                }
                //Console.WriteLine(test.ToString());
                if (test.Contains(Path.GetExtension(e.FullPath)))
                {
                    string file = null;
                    while (true)
                    {
                        try
                        {
                            using (var sr = new StreamReader(e.FullPath, Encoding.Default))
                            {
                                file = sr.ReadToEnd();
                                break;
                            }
                        }
                        catch
                        {

                        }
                    }

                    string HashFile = (Encryption.FileToHash(file));
                    if (HashFile != "")
                    {
                        //Console.WriteLine(e.Name);
                        // FileInfo fileinfo = new FileInfo(e.FullPath).LastWriteTime;
                        //Logger.Log(String.Format("File changed Path:{0} , Name :{1} HASH:{2}, EXT:{3} , Date:{4}", e.FullPath, e.Name, HashFile, Path.GetExtension(e.FullPath), new FileInfo(e.FullPath).LastWriteTime));
                        //   Logger.Log(String.Format("File changed Path:{0} , Name :{1} HASH:{2}, EXT:{3}", e.FullPath, e.Name, HashFile, Path.GetExtension(e.FullPath)));
                        // Logger.Log(String.Format("File changed Path:{0} , Name :{1} HASH:{2}", e.FullPath, e.Name, HashFile));

                        //send to serrwer
                        //wyslji na serwer jesli nie znajdzie wyslji do kolejki , 
                        //jelsi polaczenie wroci wyslji z kolejki na seerwer
                        FileEntity fe = new FileEntity(e.Name, HashFile, new FileInfo(e.FullPath).LastWriteTime, Environment.UserName, Environment.MachineName);
                        FileEntity files = new FileEntity();
                        files.FileContent = file;
                        files.hashFile = HashFile;
                       // Console.WriteLine(e.Name + " : " + new FileInfo(e.FullPath).LastWriteTime);

                        try
                        {
                            IMonitoring mc = ConnectService.getConnectWCF();
                            mc.checkConnect();
                            int x = QueueSendToSerwer.Count();
                            Console.WriteLine("Ilosc w kolejce:"+x);
                            for (int i = 0; i < x; i++)
                            
                            {

                                FileEntity tmpque = QueueSendToSerwer.Dequeue();
                                Console.WriteLine("RE:" + tmpque.nameFile);
                                string tmp = tmpque.FileContent;
                                tmpque.FileContent = null;
                                if (mc.AddFile(tmpque, ConnectService.getSetting()))
                                {
                                    FileEntity tmpFE = new FileEntity();
                                    tmpFE.FileContent = tmp;
                                    tmpFE.hashFile = tmpque.hashFile;
                                    mc.UpdateFile(tmpFE);
                                }
                            }

                            if (mc.AddFile(fe, ConnectService.getSetting()))
                            {
                                mc.UpdateFile(files);
                            }
                        }
                        catch (Exception se)
                        {
                            Console.WriteLine(se.Message);
                            Console.WriteLine("Wrzucam do kolejki:"+fe.nameFile);
                            fe.FileContent = file;
                            QueueSendToSerwer.Enqueue(fe);
                        }
                    }
                }
                //}
                //catch
                //{

                // }
           // }
          // else
         //  {
             //  let = false;

           //}
        }
    }
}
