﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace MonitoringKlient
{
    public class Encryption
    {
        private static Object thisLock = new Object();
        public static string FileToHash(string strings)
        {

          
                try
                {
                    using (var md5 = MD5.Create())
                    {
                            var hash = md5.ComputeHash(System.Text.Encoding.ASCII.GetBytes(strings));
                            return BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
                       
                    }
                }
                catch (IOException ioException)
                {
                    return "";
                }
            


        }
    }
}
