﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using MonitoringSerwerWCF;
using System.ServiceModel.Description;
using System.ServiceModel;
using System.ServiceModel.Discovery;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Discovery;


namespace MoniktoringSerwerHost
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var host = new ServiceHost(typeof(MonitoringService)))
            {
                host.Open();
                Console.WriteLine("The host is listening at:");
                foreach (var item in host.Description.Endpoints)
                {
                    Console.WriteLine("{0}\n\t{1}", item.Address, item.Binding);
                }
                Console.WriteLine();
                Console.ReadLine();
            }

        }
    }
}